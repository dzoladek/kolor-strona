﻿using EventPage.Data.Migrations;
using EventPage.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EventPage.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class RaffleAPIController : Controller
    {
        private readonly DataDbContext _context;

        public RaffleAPIController(DataDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            ViewBag.Rewards = await _context.Rewards.Include(r => r.Raffle).ToListAsync();
            ViewBag.Users = await _context.SurveyUser.Include(x=>x.SurveyResults).ToListAsync();

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Save(Raffle raffle)
        {
            var success = false;
            if (ModelState.IsValid)
            {
                _context.Add(raffle);
                await _context.SaveChangesAsync();
                success = true;
            }
            return new JsonResult(new { success = success });
        }
    }
}
