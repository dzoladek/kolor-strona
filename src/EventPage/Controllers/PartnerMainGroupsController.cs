using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EventPage.Data.Migrations;
using EventPage.Models;
using Microsoft.AspNetCore.Authorization;

namespace EventPage.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class PartnerMainGroupsController : Controller
    {
        private readonly DataDbContext _context;

        public PartnerMainGroupsController(DataDbContext context)
        {
            _context = context;    
        }

        // GET: PartnerMainGroups
        public async Task<IActionResult> Index()
        {
            return View(await _context.PartnerMainGroup.ToListAsync());
        }

        // GET: PartnerMainGroups/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var partnerMainGroup = await _context.PartnerMainGroup.SingleOrDefaultAsync(m => m.PartnerMainGroupID == id);
            if (partnerMainGroup == null)
            {
                return NotFound();
            }

            return View(partnerMainGroup);
        }

        // GET: PartnerMainGroups/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: PartnerMainGroups/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PartnerMainGroupID,Name")] PartnerMainGroup partnerMainGroup)
        {
            if (ModelState.IsValid)
            {
                _context.Add(partnerMainGroup);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(partnerMainGroup);
        }

        // GET: PartnerMainGroups/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var partnerMainGroup = await _context.PartnerMainGroup.SingleOrDefaultAsync(m => m.PartnerMainGroupID == id);
            if (partnerMainGroup == null)
            {
                return NotFound();
            }
            return View(partnerMainGroup);
        }

        // POST: PartnerMainGroups/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PartnerMainGroupID,Name")] PartnerMainGroup partnerMainGroup)
        {
            if (id != partnerMainGroup.PartnerMainGroupID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(partnerMainGroup);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PartnerMainGroupExists(partnerMainGroup.PartnerMainGroupID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(partnerMainGroup);
        }

        // GET: PartnerMainGroups/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var partnerMainGroup = await _context.PartnerMainGroup.SingleOrDefaultAsync(m => m.PartnerMainGroupID == id);
            if (partnerMainGroup == null)
            {
                return NotFound();
            }

            return View(partnerMainGroup);
        }

        // POST: PartnerMainGroups/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var partnerMainGroup = await _context.PartnerMainGroup.SingleOrDefaultAsync(m => m.PartnerMainGroupID == id);
            _context.PartnerMainGroup.Remove(partnerMainGroup);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool PartnerMainGroupExists(int id)
        {
            return _context.PartnerMainGroup.Any(e => e.PartnerMainGroupID == id);
        }
    }
}
