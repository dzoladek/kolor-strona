using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EventPage.Data.Migrations;
using EventPage.Models;
using Microsoft.AspNetCore.Hosting;
using EventPage.Helpers.ControllersHelpers;
using Microsoft.AspNetCore.Http;
using static EventPage.Enums.FileTypes;
using Microsoft.AspNetCore.Authorization;

namespace EventPage.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class PartnersController : Controller
    {
        private readonly DataDbContext _context;
        private IHostingEnvironment _hostEnv;

        public PartnersController(DataDbContext context, IHostingEnvironment appEnv)
        {
            _context = context;
            _hostEnv = appEnv;
        }

        // GET: Partners
        public async Task<IActionResult> Index()
        {
            var dataDbContext = _context.Partner.Include(p => p.PartnerGroup);
            return View(await dataDbContext.ToListAsync());
        }

        // GET: Partners/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var partner = await _context.Partner.SingleOrDefaultAsync(m => m.PartnerID == id);
            if (partner == null)
            {
                return NotFound();
            }

            return View(partner);
        }

        // GET: Partners/Create
        public IActionResult Create()
        {
            ViewData["PartnerGroupID"] = new SelectList(_context.PartnerGroup, "PartnerGroupID", "Name");
            return View();
        }

        // POST: Partners/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create( Partner partner, IFormFile file)
        {
            if(ModelState.IsValid && Request.HasFormContentType)
            {
                if (file != null)
                {
                    var path = ImageUploader.ImageUpload(_hostEnv, file, "upload", FileType.Images);
                    if (path != null)
                        partner.FilePath = path;
                }

                _context.Add(partner);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(partner);
        }

        // GET: Partners/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var partner = await _context.Partner.SingleOrDefaultAsync(m => m.PartnerID == id);
            if (partner == null)
            {
                return NotFound();
            }
            ViewData["PartnerGroupID"] = new SelectList(_context.PartnerGroup, "PartnerGroupID", "Name", partner.PartnerGroup);
            return View(partner);
        }

        // POST: Partners/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Partner partner, IFormFile file)
        {
            if (id != partner.PartnerID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    var path = ImageUploader.ImageUpload(_hostEnv, file, "upload", FileType.Images);
                    if (path != null)
                        partner.FilePath = path;
                }
                try
                {
                    _context.Update(partner);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PartnerExists(partner.PartnerID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["PartnerGroupID"] = new SelectList(_context.PartnerGroup, "PartnerGroupID", "Name", partner.PartnerGroup);
            return View(partner);
        }

        // GET: Partners/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var partner = await _context.Partner.SingleOrDefaultAsync(m => m.PartnerID == id);
            if (partner == null)
            {
                return NotFound();
            }

            return View(partner);
        }

        // POST: Partners/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var partner = await _context.Partner.SingleOrDefaultAsync(m => m.PartnerID == id);
            _context.Partner.Remove(partner);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool PartnerExists(int id)
        {
            return _context.Partner.Any(e => e.PartnerID == id);
        }
    }
}
