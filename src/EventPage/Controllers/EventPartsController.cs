using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EventPage.Data.Migrations;
using EventPage.Models;
using Microsoft.AspNetCore.Authorization;

namespace EventPage.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class EventPartsController : Controller
    {
        private readonly DataDbContext _context;

        public EventPartsController(DataDbContext context)
        {
            _context = context;    
        }

        // GET: EventParts
        public async Task<IActionResult> Index()
        {
            var dataDbContext = _context.EventPart.Include(e => e.Agenda).Include(e => e.Person).Include(b=>b.Agenda).OrderBy(g=>g.value).OrderBy(h=>h.Agenda.Name);
            return View(await dataDbContext.ToListAsync());
        }

        // GET: EventParts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var eventPart = await _context.EventPart.SingleOrDefaultAsync(m => m.EventPartID == id);
            if (eventPart == null)
            {
                return NotFound();
            }

            return View(eventPart);
        }

        // GET: EventParts/Create
        public IActionResult Create()
        {
            ViewData["AgendaID"] = new SelectList(_context.Agenda, "AgendaID", "Date");
            ViewData["PersonID"] = new SelectList(_context.Person, "PersonID", "About");
            return View();
        }

        // POST: EventParts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("EventPartID,AgendaID,Hour,Name,PersonID")] EventPart eventPart)
        {
            if (ModelState.IsValid)
            {
                _context.Add(eventPart);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["AgendaID"] = new SelectList(_context.Agenda, "AgendaID", "Date", eventPart.AgendaID);
            ViewData["PersonID"] = new SelectList(_context.Person, "PersonID", "About", eventPart.PersonID);
            return View(eventPart);
        }

        // GET: EventParts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var eventPart = await _context.EventPart.SingleOrDefaultAsync(m => m.EventPartID == id);
            if (eventPart == null)
            {
                return NotFound();
            }
            ViewData["AgendaID"] = new SelectList(_context.Agenda, "AgendaID", "Date", eventPart.AgendaID);
            ViewData["PersonID"] = new SelectList(_context.Person, "PersonID", "About", eventPart.PersonID);
            return View(eventPart);
        }

        // POST: EventParts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("EventPartID,AgendaID,Hour,Name,PersonID")] EventPart eventPart)
        {
            if (id != eventPart.EventPartID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(eventPart);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EventPartExists(eventPart.EventPartID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["AgendaID"] = new SelectList(_context.Agenda, "AgendaID", "Date", eventPart.AgendaID);
            ViewData["PersonID"] = new SelectList(_context.Person, "PersonID", "About", eventPart.PersonID);
            return View(eventPart);
        }

        // GET: EventParts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var eventPart = await _context.EventPart.SingleOrDefaultAsync(m => m.EventPartID == id);
            if (eventPart == null)
            {
                return NotFound();
            }

            return View(eventPart);
        }

        // POST: EventParts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var eventPart = await _context.EventPart.SingleOrDefaultAsync(m => m.EventPartID == id);
            _context.EventPart.Remove(eventPart);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool EventPartExists(int id)
        {
            return _context.EventPart.Any(e => e.EventPartID == id);
        }
    }
}
