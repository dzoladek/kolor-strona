using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EventPage.Data.Migrations;
using EventPage.Models;
using Microsoft.AspNetCore.Authorization;

namespace EventPage.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class SurveyResultsController : Controller
    {
        private readonly DataDbContext _context;

        public SurveyResultsController(DataDbContext context)
        {
            _context = context;    
        }

        // GET: SurveyResults
        public async Task<IActionResult> Index()
        {
            var dataDbContext = _context.SurveyResult.Include(s => s.EventPart).Include(s => s.SurveyUser);
            return View(await dataDbContext.ToListAsync());
        }

        // GET: SurveyResults/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var surveyResult = await _context.SurveyResult.SingleOrDefaultAsync(m => m.SurveyResultID == id);
            if (surveyResult == null)
            {
                return NotFound();
            }

            return View(surveyResult);
        }

        // GET: SurveyResults/Create
        public IActionResult Create()
        {
            ViewData["EventPartID"] = new SelectList(_context.EventPart, "EventPartID", "Hour");
            ViewData["SurveyUserID"] = new SelectList(_context.SurveyUser, "SurveyUserID", "Email");
            return View();
        }

        // POST: SurveyResults/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SurveyResultID,Cons,EventPartID,Pros,SurveyUserID")] SurveyResult surveyResult)
        {
            if (ModelState.IsValid)
            {
                _context.Add(surveyResult);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["EventPartID"] = new SelectList(_context.EventPart, "EventPartID", "Hour", surveyResult.EventPartID);
            ViewData["SurveyUserID"] = new SelectList(_context.SurveyUser, "SurveyUserID", "Email", surveyResult.SurveyUserID);
            return View(surveyResult);
        }

        // GET: SurveyResults/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var surveyResult = await _context.SurveyResult.SingleOrDefaultAsync(m => m.SurveyResultID == id);
            if (surveyResult == null)
            {
                return NotFound();
            }
            ViewData["EventPartID"] = new SelectList(_context.EventPart, "EventPartID", "Hour", surveyResult.EventPartID);
            ViewData["SurveyUserID"] = new SelectList(_context.SurveyUser, "SurveyUserID", "Email", surveyResult.SurveyUserID);
            return View(surveyResult);
        }

        // POST: SurveyResults/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("SurveyResultID,Cons,EventPartID,Pros,SurveyUserID")] SurveyResult surveyResult)
        {
            if (id != surveyResult.SurveyResultID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(surveyResult);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SurveyResultExists(surveyResult.SurveyResultID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["EventPartID"] = new SelectList(_context.EventPart, "EventPartID", "Hour", surveyResult.EventPartID);
            ViewData["SurveyUserID"] = new SelectList(_context.SurveyUser, "SurveyUserID", "Email", surveyResult.SurveyUserID);
            return View(surveyResult);
        }

        // GET: SurveyResults/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var surveyResult = await _context.SurveyResult.SingleOrDefaultAsync(m => m.SurveyResultID == id);
            if (surveyResult == null)
            {
                return NotFound();
            }

            return View(surveyResult);
        }

        // POST: SurveyResults/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var surveyResult = await _context.SurveyResult.SingleOrDefaultAsync(m => m.SurveyResultID == id);
            _context.SurveyResult.Remove(surveyResult);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool SurveyResultExists(int id)
        {
            return _context.SurveyResult.Any(e => e.SurveyResultID == id);
        }
    }
}
