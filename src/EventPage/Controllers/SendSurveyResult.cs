﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using EventPage.Models;
using EventPage.Data.Migrations;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace EventPage.Controllers
{
    public class SendSurveyResult : Controller
    {
        private readonly DataDbContext _context;

        public SendSurveyResult(DataDbContext context)
        {
            _context = context;
        }

        [HttpPost]
        public async Task<ActionResult> SendNew(SurveyUser u , SurveyResult r)
        {
            u.Cookie = u.Cookie ?? Guid.NewGuid().ToString();
            bool alreadyFilled = false;
            try
            {
                if (ModelState.IsValid)
                {
                    SurveyUser alreadyAdded = _context.SurveyUser.Where(x => x.Id == u.Id).FirstOrDefault();

                    if (alreadyAdded == null)
                    {
                        _context.Add(u);
                        await _context.SaveChangesAsync();
                    }
                    else if (alreadyAdded.Email == u.Email)
                        u = alreadyAdded;
                }

                SurveyUser user = _context.SurveyUser.Where(x => x.Cookie == u.Cookie).FirstOrDefault();

                if(user == null)
                {
                    u.Cookie = "-1";
                    throw new Exception("User not found");
                }
                SurveyResult alreadyFilledSurvey = _context.SurveyResult.Where(x => x.EventPartID == r.EventPartID && x.SurveyUserID == user.SurveyUserID).FirstOrDefault();
                if ( alreadyFilledSurvey != null)
                {
                    alreadyFilled = true;
                    throw new Exception("You already filled this survey");
                }

                r.SurveyUserID = user.SurveyUserID;
                _context.Add(r);
                await _context.SaveChangesAsync();

                return new JsonResult(new { Success = true, Cookie = u.Cookie});

            }
            catch (Exception e)
            {
                return new JsonResult(new { Success = false, error = e.ToString(), Cookie = u.Cookie ,alreadyFilled = alreadyFilled });
            }
        }

    }
}
