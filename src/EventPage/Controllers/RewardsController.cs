using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EventPage.Data.Migrations;
using EventPage.Models;
using EventPage.Helpers.ControllersHelpers;
using Microsoft.AspNetCore.Hosting;
using static EventPage.Enums.FileTypes;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;

namespace EventPage.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class RewardsController : Controller
    {
        private readonly DataDbContext _context;
        private IHostingEnvironment _hostEnv;

        public RewardsController(DataDbContext context, IHostingEnvironment appEnv)
        {
            _context = context;
            _hostEnv = appEnv;
        }


        // GET: Rewards
        public async Task<IActionResult> Index()
        {
            return View(await _context.Rewards.ToListAsync());
        }

        // GET: Rewards/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rewards = await _context.Rewards.SingleOrDefaultAsync(m => m.RewardsID == id);
            if (rewards == null)
            {
                return NotFound();
            }

            return View(rewards);
        }

        // GET: Rewards/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Rewards/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("RewardsID,ImagePath,Name")] Rewards rewards, IFormFile file)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    var path = ImageUploader.ImageUpload(_hostEnv, file, "upload", FileType.Images);
                    if (path != null)
                        rewards.ImagePath = path;
                }
                _context.Add(rewards);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(rewards);
        }

        // GET: Rewards/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rewards = await _context.Rewards.SingleOrDefaultAsync(m => m.RewardsID == id);
            if (rewards == null)
            {
                return NotFound();
            }
            return View(rewards);
        }

        // POST: Rewards/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("RewardsID,ImagePath,Name")] Rewards rewards, IFormFile file)
        {
            if (id != rewards.RewardsID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (file != null)
                    {
                        var path = ImageUploader.ImageUpload(_hostEnv, file, "upload", FileType.Images);
                        if (path != null)
                            rewards.ImagePath = path;
                    }

                    _context.Update(rewards);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RewardsExists(rewards.RewardsID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(rewards);
        }

        // GET: Rewards/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rewards = await _context.Rewards.SingleOrDefaultAsync(m => m.RewardsID == id);
            if (rewards == null)
            {
                return NotFound();
            }

            return View(rewards);
        }

        // POST: Rewards/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var rewards = await _context.Rewards.SingleOrDefaultAsync(m => m.RewardsID == id);
            _context.Rewards.Remove(rewards);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool RewardsExists(int id)
        {
            return _context.Rewards.Any(e => e.RewardsID == id);
        }
    }
}
