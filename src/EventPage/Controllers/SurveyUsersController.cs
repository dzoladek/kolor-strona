using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EventPage.Data.Migrations;
using EventPage.Models;
using Microsoft.AspNetCore.Authorization;

namespace EventPage.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class SurveyUsersController : Controller
    {
        private readonly DataDbContext _context;

        public SurveyUsersController(DataDbContext context)
        {
            _context = context;    
        }

        // GET: SurveyUsers
        public async Task<IActionResult> Index()
        {
            return View(await _context.SurveyUser.ToListAsync());
        }

        // GET: SurveyUsers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var surveyUser = await _context.SurveyUser.SingleOrDefaultAsync(m => m.SurveyUserID == id);
            if (surveyUser == null)
            {
                return NotFound();
            }

            return View(surveyUser);
        }

        // GET: SurveyUsers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: SurveyUsers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SurveyUserID,Cookie,Email,FullName,Id,Status")] SurveyUser surveyUser)
        {
            if (ModelState.IsValid)
            {
                _context.Add(surveyUser);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(surveyUser);
        }

        // GET: SurveyUsers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var surveyUser = await _context.SurveyUser.SingleOrDefaultAsync(m => m.SurveyUserID == id);
            if (surveyUser == null)
            {
                return NotFound();
            }
            return View(surveyUser);
        }

        // POST: SurveyUsers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("SurveyUserID,Cookie,Email,FullName,Id,Status")] SurveyUser surveyUser)
        {
            if (id != surveyUser.SurveyUserID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(surveyUser);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SurveyUserExists(surveyUser.SurveyUserID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(surveyUser);
        }

        // GET: SurveyUsers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var surveyUser = await _context.SurveyUser.SingleOrDefaultAsync(m => m.SurveyUserID == id);
            if (surveyUser == null)
            {
                return NotFound();
            }

            return View(surveyUser);
        }

        // POST: SurveyUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var surveyUser = await _context.SurveyUser.SingleOrDefaultAsync(m => m.SurveyUserID == id);
            _context.SurveyUser.Remove(surveyUser);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("DeleteAll")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteAllConfirmed()
        {
            var surveyUsers = await _context.SurveyUser.ToListAsync();
            _context.SurveyUser.RemoveRange(surveyUsers);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        public IActionResult DeleteAll()
        {
            return View();
        }

        private bool SurveyUserExists(int id)
        {
            return _context.SurveyUser.Any(e => e.SurveyUserID == id);
        }
    }
}
