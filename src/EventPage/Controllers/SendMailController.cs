﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace EventPage.Controllers
{
    public class SendMailController
    {
        [HttpPost]
        public ActionResult SendNew(IFormCollection data)
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.To.Add("ktrubisz@outlook.com");
                mail.From = new MailAddress("pokochajug@outlook.com");
                mail.Subject = "Message from website";
                mail.SubjectEncoding = System.Text.Encoding.UTF8;
                mail.Body = data["email"] + "</br>" + data["message"] + "</br>" + data["name"];
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.IsBodyHtml = true;
                var client = new SmtpClient
                {
                    Port = 587,
                    Host = "smtp-mail.outlook.com",
                    EnableSsl = true,
                    UseDefaultCredentials = false
                };
                client.Credentials = new System.Net.NetworkCredential("pokochajug@outlook.com", "Policja1234");
                client.Send(mail);
                return new JsonResult(new { Success = true });
            }
            catch (Exception e)
            {
                return new JsonResult(new { Success = false, error = e });
            }
        }
        
    }
}
