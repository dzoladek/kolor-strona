using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EventPage.Data.Migrations;
using EventPage.Models;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Authorization;

namespace EventPage.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class PartnerGroupsController : Controller
    {
        private readonly DataDbContext _context;

        public PartnerGroupsController(DataDbContext context)
        {
            _context = context;    
        }

        // GET: PartnerGroups
        public async Task<IActionResult> Index()
        {
            var dataDbContext = _context.PartnerGroup.Include(p => p.PartnerMainGroup);
            return View(await dataDbContext.ToListAsync());
        }

        // GET: PartnerGroups/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var partnerGroup = await _context.PartnerGroup.SingleOrDefaultAsync(m => m.PartnerGroupID == id);
            if (partnerGroup == null)
            {
                return NotFound();
            }

            return View(partnerGroup);
        }

        // GET: PartnerGroups/Create
        public IActionResult Create()
        {
            ViewData["PartnerMainGroupID"] = new SelectList(_context.PartnerMainGroup, "PartnerMainGroupID", "Name");
            return View();
        }

        // POST: PartnerGroups/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PartnerGroupID,LogoSize,Name,PartnerMainGroupID")] PartnerGroup partnerGroup)
        {
            partnerGroup.PartnerMainGroup = _context.PartnerMainGroup.Where(x => x.PartnerMainGroupID == partnerGroup.PartnerMainGroupID).First();
            var results = new List<ValidationResult>();
            var context = new ValidationContext(partnerGroup, null, null);
            if (Validator.TryValidateObject(partnerGroup,context,results))
            {
                _context.Add(partnerGroup);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["PartnerMainGroupID"] = new SelectList(_context.PartnerMainGroup, "PartnerMainGroupID", "Name", partnerGroup.PartnerMainGroupID);
            return View(partnerGroup);
        }

        // GET: PartnerGroups/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var partnerGroup = await _context.PartnerGroup.SingleOrDefaultAsync(m => m.PartnerGroupID == id);
            if (partnerGroup == null)
            {
                return NotFound();
            }
            ViewData["PartnerMainGroupID"] = new SelectList(_context.PartnerMainGroup, "PartnerMainGroupID", "Name", partnerGroup.PartnerMainGroupID);
            return View(partnerGroup);
        }

        // POST: PartnerGroups/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PartnerGroupID,LogoSize,Name,PartnerMainGroupID")] PartnerGroup partnerGroup)
        {
            if (id != partnerGroup.PartnerGroupID)
            {
                return NotFound();
            }

            partnerGroup.PartnerMainGroup = _context.PartnerMainGroup.Where(x => x.PartnerMainGroupID == partnerGroup.PartnerMainGroupID).First();
            var results = new List<ValidationResult>();
            var context = new ValidationContext(partnerGroup, null, null);
            if (Validator.TryValidateObject(partnerGroup, context, results))
            {
                try
                {
                    _context.Update(partnerGroup);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PartnerGroupExists(partnerGroup.PartnerGroupID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["PartnerMainGroupID"] = new SelectList(_context.PartnerMainGroup, "PartnerMainGroupID", "Name", partnerGroup.PartnerMainGroupID);
            return View(partnerGroup);
        }

        // GET: PartnerGroups/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var partnerGroup = await _context.PartnerGroup.SingleOrDefaultAsync(m => m.PartnerGroupID == id);
            if (partnerGroup == null)
            {
                return NotFound();
            }

            return View(partnerGroup);
        }

        // POST: PartnerGroups/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var partnerGroup = await _context.PartnerGroup.SingleOrDefaultAsync(m => m.PartnerGroupID == id);
            _context.PartnerGroup.Remove(partnerGroup);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool PartnerGroupExists(int id)
        {
            return _context.PartnerGroup.Any(e => e.PartnerGroupID == id);
        }
    }
}
