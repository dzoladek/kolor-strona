﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EventPage.Data.Migrations;
using Microsoft.EntityFrameworkCore;
using EventPage.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace EventPage.Controllers
{
    public class HomeController : Controller
    {
        private readonly DataDbContext _context;

        public HomeController(DataDbContext context)
        {
            _context = context;
        }

        // GET: Agenda
        public async Task<IActionResult> Index()
        {
            var partners = await _context.PartnerMainGroup.Include(x => x.PartnerGroups).ThenInclude(y => y.Partners).ToListAsync();
            ViewData["PartnerGroup"] = partners;
            ViewData["Persons"] = await _context.Person.Where(x => x.EventParts.Count > 0).ToListAsync();
            ViewData["Agenda"] = await _context.Agenda.Where(a=>a.EventParts.Count > 0).Include(x=>x.EventParts).ThenInclude(c=> c.Person).ToListAsync();
            ViewData["Organisers"] = await _context.Person.Where(x => x.Organizer == true).ToListAsync();
            ViewData["Prelections"] = new SelectList(_context.EventPart.Where(m=>m.PersonID != null), "EventPartID", "Description");
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
        public IActionResult Ankiety()
        {
            ViewData["Prelections"] = new SelectList(_context.EventPart.Where(m => m.PersonID != null), "EventPartID", "Description");
            return View();
        }
    }
}
