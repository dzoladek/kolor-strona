using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EventPage.Data.Migrations;
using EventPage.Models;
using Microsoft.AspNetCore.Authorization;

namespace EventPage.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class RafflesController : Controller
    {
        private readonly DataDbContext _context;

        public RafflesController(DataDbContext context)
        {
            _context = context;    
        }

        // GET: Raffles
        public async Task<IActionResult> Index()
        {
            var dataDbContext = _context.Raffle.Include(r => r.Reward).Include(r => r.Winner);
            return View(await dataDbContext.ToListAsync());
        }

        // GET: Raffles/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var raffle = await _context.Raffle.SingleOrDefaultAsync(m => m.RaffleID == id);
            if (raffle == null)
            {
                return NotFound();
            }

            return View(raffle);
        }

        // GET: Raffles/Create
        public IActionResult Create()
        {
            ViewData["RewardID"] = new SelectList(_context.Rewards, "RewardsID", "Name");
            ViewData["WinnerID"] = new SelectList(_context.SurveyUser, "SurveyUserID", "Email");
            return View();
        }

        // POST: Raffles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("RaffleID,RewardID,WinnerID")] Raffle raffle)
        {
            if (ModelState.IsValid)
            {
                _context.Add(raffle);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["RewardID"] = new SelectList(_context.Rewards, "RewardsID", "Name", raffle.RewardID);
            ViewData["WinnerID"] = new SelectList(_context.SurveyUser, "SurveyUserID", "Email", raffle.WinnerID);
            return View(raffle);
        }

        // GET: Raffles/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var raffle = await _context.Raffle.SingleOrDefaultAsync(m => m.RaffleID == id);
            if (raffle == null)
            {
                return NotFound();
            }
            ViewData["RewardID"] = new SelectList(_context.Rewards, "RewardsID", "Name", raffle.RewardID);
            ViewData["WinnerID"] = new SelectList(_context.SurveyUser, "SurveyUserID", "Email", raffle.WinnerID);
            return View(raffle);
        }

        // POST: Raffles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("RaffleID,RewardID,WinnerID")] Raffle raffle)
        {
            if (id != raffle.RaffleID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(raffle);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RaffleExists(raffle.RaffleID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["RewardID"] = new SelectList(_context.Rewards, "RewardsID", "Name", raffle.RewardID);
            ViewData["WinnerID"] = new SelectList(_context.SurveyUser, "SurveyUserID", "Email", raffle.WinnerID);
            return View(raffle);
        }

        // GET: Raffles/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var raffle = await _context.Raffle.SingleOrDefaultAsync(m => m.RaffleID == id);
            if (raffle == null)
            {
                return NotFound();
            }

            return View(raffle);
        }

        // POST: Raffles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var raffle = await _context.Raffle.SingleOrDefaultAsync(m => m.RaffleID == id);
            _context.Raffle.Remove(raffle);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool RaffleExists(int id)
        {
            return _context.Raffle.Any(e => e.RaffleID == id);
        }
    }
}
