﻿using EventPage.Models;
using Microsoft.EntityFrameworkCore;

namespace EventPage.Data.Migrations
{
    public class DataDbContext : DbContext
    {
        public DataDbContext(DbContextOptions<DataDbContext> options) : base(options)
        {
        }

        public DataDbContext() : base()
        {

        }

        public virtual DbSet<Agenda> Agenda { get; set; }
        public virtual DbSet<EventPart> EventPart { get; set; }
        public virtual DbSet<Partner> Partner { get; set; }
        public virtual DbSet<PartnerGroup> PartnerGroup { get; set; }
        public virtual DbSet<Person> Person { get; set; }
        public virtual DbSet<Raffle> Raffle { get; set; }
        public virtual DbSet<Rewards> Rewards { get; set; }
        public virtual DbSet<SurveyResult> SurveyResult { get; set; }
        public virtual DbSet<PartnerMainGroup> PartnerMainGroup { get; set; }
        public virtual DbSet<SurveyUser> SurveyUser { get; set; }
    }
}
