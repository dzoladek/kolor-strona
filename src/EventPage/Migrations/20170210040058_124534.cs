﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EventPage.Migrations
{
    public partial class _124534 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Raffle_SurveyResult_WinnerID",
                table: "Raffle");

            migrationBuilder.AddForeignKey(
                name: "FK_Raffle_SurveyUser_WinnerID",
                table: "Raffle",
                column: "WinnerID",
                principalTable: "SurveyUser",
                principalColumn: "SurveyUserID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Raffle_SurveyUser_WinnerID",
                table: "Raffle");

            migrationBuilder.AddForeignKey(
                name: "FK_Raffle_SurveyResult_WinnerID",
                table: "Raffle",
                column: "WinnerID",
                principalTable: "SurveyResult",
                principalColumn: "SurveyResultID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
