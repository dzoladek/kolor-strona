﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace EventPage.Migrations
{
    public partial class _12434 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.DropTable(
    name: "Raffle");
            migrationBuilder.DropTable(
    name: "SurveyResult");

            //migrationBuilder.CreateTable(
            //    name: "Agenda",
            //    columns: table => new
            //    {
            //        AgendaID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        Date = table.Column<string>(nullable: false),
            //        Name = table.Column<string>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Agenda", x => x.AgendaID);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "PartnerMainGroup",
            //    columns: table => new
            //    {
            //        PartnerMainGroupID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        Name = table.Column<string>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_PartnerMainGroup", x => x.PartnerMainGroupID);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Person",
            //    columns: table => new
            //    {
            //        PersonID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        About = table.Column<string>(nullable: false),
            //        Forname = table.Column<string>(nullable: false),
            //        ImagePath = table.Column<string>(nullable: true),
            //        Organizer = table.Column<bool>(nullable: false),
            //        Surname = table.Column<string>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Person", x => x.PersonID);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Rewards",
            //    columns: table => new
            //    {
            //        RewardsID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        ImagePath = table.Column<string>(nullable: true),
            //        Name = table.Column<string>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Rewards", x => x.RewardsID);
            //    });

            migrationBuilder.CreateTable(
                name: "SurveyUser",
                columns: table => new
                {
                    SurveyUserID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Cookie = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: false),
                    FullName = table.Column<string>(nullable: false),
                    Id = table.Column<int>(nullable: false),
                    Status = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SurveyUser", x => x.SurveyUserID);
                });

            //migrationBuilder.CreateTable(
            //    name: "PartnerGroup",
            //    columns: table => new
            //    {
            //        PartnerGroupID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        LogoSize = table.Column<string>(nullable: false),
            //        Name = table.Column<string>(nullable: false),
            //        PartnerMainGroupID = table.Column<int>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_PartnerGroup", x => x.PartnerGroupID);
            //        table.ForeignKey(
            //            name: "FK_PartnerGroup_PartnerMainGroup_PartnerMainGroupID",
            //            column: x => x.PartnerMainGroupID,
            //            principalTable: "PartnerMainGroup",
            //            principalColumn: "PartnerMainGroupID",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "EventPart",
            //    columns: table => new
            //    {
            //        EventPartID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        AgendaID = table.Column<int>(nullable: false),
            //        Hour = table.Column<string>(nullable: false),
            //        Name = table.Column<string>(nullable: false),
            //        PersonID = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_EventPart", x => x.EventPartID);
            //        table.ForeignKey(
            //            name: "FK_EventPart_Agenda_AgendaID",
            //            column: x => x.AgendaID,
            //            principalTable: "Agenda",
            //            principalColumn: "AgendaID",
            //            onDelete: ReferentialAction.Cascade);
            //        table.ForeignKey(
            //            name: "FK_EventPart_Person_PersonID",
            //            column: x => x.PersonID,
            //            principalTable: "Person",
            //            principalColumn: "PersonID",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Partner",
            //    columns: table => new
            //    {
            //        PartnerID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        FilePath = table.Column<string>(nullable: true),
            //        Name = table.Column<string>(nullable: false),
            //        PartnerGroupID = table.Column<int>(nullable: false),
            //        Website = table.Column<string>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Partner", x => x.PartnerID);
            //        table.ForeignKey(
            //            name: "FK_Partner_PartnerGroup_PartnerGroupID",
            //            column: x => x.PartnerGroupID,
            //            principalTable: "PartnerGroup",
            //            principalColumn: "PartnerGroupID",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            migrationBuilder.CreateTable(
                name: "SurveyResult",
                columns: table => new
                {
                    SurveyResultID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Cons = table.Column<string>(nullable: false),
                    EventPartID = table.Column<int>(nullable: false),
                    Pros = table.Column<string>(nullable: false),
                    SurveyUserID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SurveyResult", x => x.SurveyResultID);
                    table.ForeignKey(
                        name: "FK_SurveyResult_EventPart_EventPartID",
                        column: x => x.EventPartID,
                        principalTable: "EventPart",
                        principalColumn: "EventPartID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SurveyResult_SurveyUser_SurveyUserID",
                        column: x => x.SurveyUserID,
                        principalTable: "SurveyUser",
                        principalColumn: "SurveyUserID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Raffle",
                columns: table => new
                {
                    RaffleID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RewardID = table.Column<int>(nullable: false),
                    WinnerID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Raffle", x => x.RaffleID);
                    table.ForeignKey(
                        name: "FK_Raffle_Rewards_RewardID",
                        column: x => x.RewardID,
                        principalTable: "Rewards",
                        principalColumn: "RewardsID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Raffle_SurveyResult_WinnerID",
                        column: x => x.WinnerID,
                        principalTable: "SurveyResult",
                        principalColumn: "SurveyResultID",
                        onDelete: ReferentialAction.Cascade);
                });

            //migrationBuilder.CreateIndex(
            //    name: "IX_EventPart_AgendaID",
            //    table: "EventPart",
            //    column: "AgendaID");

            //migrationBuilder.CreateIndex(
            //    name: "IX_EventPart_PersonID",
            //    table: "EventPart",
            //    column: "PersonID");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Partner_PartnerGroupID",
            //    table: "Partner",
            //    column: "PartnerGroupID");

            //migrationBuilder.CreateIndex(
            //    name: "IX_PartnerGroup_PartnerMainGroupID",
            //    table: "PartnerGroup",
            //    column: "PartnerMainGroupID");

            migrationBuilder.CreateIndex(
                name: "IX_Raffle_RewardID",
                table: "Raffle",
                column: "RewardID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Raffle_WinnerID",
                table: "Raffle",
                column: "WinnerID");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyResult_EventPartID",
                table: "SurveyResult",
                column: "EventPartID");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyResult_SurveyUserID",
                table: "SurveyResult",
                column: "SurveyUserID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
   /*         migrationBuilder.DropTable(
    name: "Partner");

            migrationBuilder.DropTable(
                name: "Raffle");

            migrationBuilder.DropTable(
                name: "PartnerGroup");

            migrationBuilder.DropTable(
                name: "Rewards");

            migrationBuilder.DropTable(
                name: "SurveyResult");

            migrationBuilder.DropTable(
                name: "PartnerMainGroup");

            migrationBuilder.DropTable(
                name: "EventPart");

            migrationBuilder.DropTable(
                name: "SurveyUser");

            migrationBuilder.DropTable(
                name: "Agenda");

            migrationBuilder.DropTable(
                name: "Person"); */
        }
    }
}
