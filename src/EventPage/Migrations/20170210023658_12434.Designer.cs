﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using EventPage.Data.Migrations;

namespace EventPage.Migrations
{
    [DbContext(typeof(DataDbContext))]
    [Migration("20170210023658_12434")]
    partial class _12434
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("EventPage.Models.Agenda", b =>
                {
                    b.Property<int>("AgendaID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Date")
                        .IsRequired();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("AgendaID");

                    b.ToTable("Agenda");
                });

            modelBuilder.Entity("EventPage.Models.EventPart", b =>
                {
                    b.Property<int>("EventPartID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AgendaID");

                    b.Property<string>("Hour")
                        .IsRequired();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<int?>("PersonID");

                    b.HasKey("EventPartID");

                    b.HasIndex("AgendaID");

                    b.HasIndex("PersonID");

                    b.ToTable("EventPart");
                });

            modelBuilder.Entity("EventPage.Models.Partner", b =>
                {
                    b.Property<int>("PartnerID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("FilePath");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<int>("PartnerGroupID");

                    b.Property<string>("Website")
                        .IsRequired();

                    b.HasKey("PartnerID");

                    b.HasIndex("PartnerGroupID");

                    b.ToTable("Partner");
                });

            modelBuilder.Entity("EventPage.Models.PartnerGroup", b =>
                {
                    b.Property<int>("PartnerGroupID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("LogoSize")
                        .IsRequired();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<int>("PartnerMainGroupID");

                    b.HasKey("PartnerGroupID");

                    b.HasIndex("PartnerMainGroupID");

                    b.ToTable("PartnerGroup");
                });

            modelBuilder.Entity("EventPage.Models.PartnerMainGroup", b =>
                {
                    b.Property<int>("PartnerMainGroupID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("PartnerMainGroupID");

                    b.ToTable("PartnerMainGroup");
                });

            modelBuilder.Entity("EventPage.Models.Person", b =>
                {
                    b.Property<int>("PersonID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("About")
                        .IsRequired();

                    b.Property<string>("Forname")
                        .IsRequired();

                    b.Property<string>("ImagePath");

                    b.Property<bool>("Organizer");

                    b.Property<string>("Surname")
                        .IsRequired();

                    b.HasKey("PersonID");

                    b.ToTable("Person");
                });

            modelBuilder.Entity("EventPage.Models.Raffle", b =>
                {
                    b.Property<int>("RaffleID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("RewardID");

                    b.Property<int>("WinnerID");

                    b.HasKey("RaffleID");

                    b.HasIndex("RewardID")
                        .IsUnique();

                    b.HasIndex("WinnerID");

                    b.ToTable("Raffle");
                });

            modelBuilder.Entity("EventPage.Models.Rewards", b =>
                {
                    b.Property<int>("RewardsID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ImagePath");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("RewardsID");

                    b.ToTable("Rewards");
                });

            modelBuilder.Entity("EventPage.Models.SurveyResult", b =>
                {
                    b.Property<int>("SurveyResultID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Cons")
                        .IsRequired();

                    b.Property<int>("EventPartID");

                    b.Property<string>("Pros")
                        .IsRequired();

                    b.Property<int>("SurveyUserID");

                    b.HasKey("SurveyResultID");

                    b.HasIndex("EventPartID");

                    b.HasIndex("SurveyUserID");

                    b.ToTable("SurveyResult");
                });

            modelBuilder.Entity("EventPage.Models.SurveyUser", b =>
                {
                    b.Property<int>("SurveyUserID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Cookie");

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<string>("FullName")
                        .IsRequired();

                    b.Property<int>("Id");

                    b.Property<string>("Status")
                        .IsRequired();

                    b.HasKey("SurveyUserID");

                    b.ToTable("SurveyUser");
                });

            modelBuilder.Entity("EventPage.Models.EventPart", b =>
                {
                    b.HasOne("EventPage.Models.Agenda", "Agenda")
                        .WithMany("EventParts")
                        .HasForeignKey("AgendaID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("EventPage.Models.Person", "Person")
                        .WithMany("EventParts")
                        .HasForeignKey("PersonID");
                });

            modelBuilder.Entity("EventPage.Models.Partner", b =>
                {
                    b.HasOne("EventPage.Models.PartnerGroup", "PartnerGroup")
                        .WithMany("Partners")
                        .HasForeignKey("PartnerGroupID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("EventPage.Models.PartnerGroup", b =>
                {
                    b.HasOne("EventPage.Models.PartnerMainGroup", "PartnerMainGroup")
                        .WithMany("PartnerGroups")
                        .HasForeignKey("PartnerMainGroupID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("EventPage.Models.Raffle", b =>
                {
                    b.HasOne("EventPage.Models.Rewards", "Reward")
                        .WithOne("Raffle")
                        .HasForeignKey("EventPage.Models.Raffle", "RewardID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("EventPage.Models.SurveyResult", "Winner")
                        .WithMany()
                        .HasForeignKey("WinnerID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("EventPage.Models.SurveyResult", b =>
                {
                    b.HasOne("EventPage.Models.EventPart", "EventPart")
                        .WithMany()
                        .HasForeignKey("EventPartID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("EventPage.Models.SurveyUser", "SurveyUser")
                        .WithMany("SurveyResults")
                        .HasForeignKey("SurveyUserID")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
