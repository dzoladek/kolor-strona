﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EventPage.Models
{
    public class PartnerGroup : IComparable<PartnerGroup>
    {
        [Key]
        public int PartnerGroupID { get; set; }
        [Required]
        public string Name { get; set; }

        [Range(1,5)]
        [Required]
        public string LogoSize { get; set; }

        public int PartnerMainGroupID { get; set; }
        public PartnerMainGroup PartnerMainGroup { get; set; }

        public ICollection<Partner> Partners { get; set; }

        public int CompareTo(PartnerGroup other)
        {
            int result = 0;
            if (other == null) result = 1;
            if (result == 0) result = PartnerMainGroup.Name.CompareTo(other.PartnerMainGroup.Name);
            if (result == 0) result = LogoSize.CompareTo(other.LogoSize);
            if (result == 0) result = Name.CompareTo(other.Name);
            return result;
        }
    }
}
