﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EventPage.Models
{
    public class SurveyResult
    {
        [Key]
        public int SurveyResultID { get; set; }

        [Required]
        public string Pros { get; set; }

        [Required]
        public string Cons { get; set; }

        [Required]
        public int SurveyUserID { get; set; }
        public SurveyUser SurveyUser { get; set; }

        [Required]
        public int EventPartID { get; set; }
        public EventPart EventPart { get; set; }

    }
}
