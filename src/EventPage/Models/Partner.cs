﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EventPage.Models
{
    public class Partner
    {
        [Key]
        public int PartnerID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Website { get; set; }

        public string FilePath { get; set; }

        public int PartnerGroupID { get; set; }
        public PartnerGroup PartnerGroup { get; set; }

    }
}
