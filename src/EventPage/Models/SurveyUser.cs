﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EventPage.Models
{
    public class SurveyUser
    {
        [Key]
        public int SurveyUserID { get; set; }

        [Required]
        public string FullName { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public int Id { get; set; }

        public string Cookie { get; set; }

        [Required]
        public string Status { get; set; }

        public ICollection<SurveyResult> SurveyResults { get; set; }

        public string OnlyForename
        {
            get
            {
                if (FullName != null)
                {
                    int spacePosition = FullName.IndexOf(' ');
                    if (spacePosition == -1)
                        return FullName;
                    return FullName.Substring(0, spacePosition + 1);
                }
                return "";
            }
        }
    }



}
