﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EventPage.Models
{
    public class PartnerMainGroup
    {
        [Key]
        public int PartnerMainGroupID { get; set; }
        [Required]
        public string Name { get; set; }

        public ICollection<PartnerGroup> PartnerGroups { get; set; }
    }
}
