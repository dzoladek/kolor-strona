﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EventPage.Models
{
    public class EventPart
    {
        [Key]
        public int EventPartID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Hour { get; set; }

        public int? PersonID { get; set; }
        public Person Person { get; set; }

        public int AgendaID { get; set; }
        public Agenda Agenda { get; set; }


        public string Description
        {
            get
            {
                var temp = (Person == null) ? "" : Person.FullName + " - ";
                return temp + Name;
            }
        }

        public int value {
            get
            {
                int val = 0;
                for (var i = 0; i < Hour.Length; i++)
                    val += (int)Math.Pow(10, Hour.Length - i) * ((Hour[i] >= '0' && Hour[i] <= '9') ? Hour[i] - '0' : 0);

                return val;
            }
        }
    }
}
