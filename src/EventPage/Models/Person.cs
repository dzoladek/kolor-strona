﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EventPage.Models
{
    public class Person
    {
        [Key]
        public int PersonID { get; set; }
        [Required]
        public string Forname { get; set; }
        [Required]
        public string Surname { get; set; }

        [Required]
        public string About { get; set; }

        public bool Organizer { get; set; }

        public string ImagePath { get; set; }

        public string FullName
        {
            get
            {
                return Forname + " " + Surname;
            }
        }

        public ICollection<EventPart> EventParts { get; set; }

    }
}
