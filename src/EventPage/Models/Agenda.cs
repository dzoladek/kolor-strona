﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EventPage.Models
{
    public class Agenda
    {
        [Key]
        public int AgendaID { get; set; }
        [Required]
        public string Date { get; set; }
        [Required]
        public string Name { get; set; }

        public ICollection<EventPart> EventParts {get; set; }
    }
}
