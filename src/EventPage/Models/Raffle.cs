﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EventPage.Models
{
    public class Raffle
    {
        [Key]
        public int RaffleID { get; set; }
        [Required]
        public int WinnerID { get; set; }
        [Required]
        public int RewardID { get; set; }


        public Rewards Reward { get; set; }
        public SurveyUser Winner { get; set; }
    }
}
