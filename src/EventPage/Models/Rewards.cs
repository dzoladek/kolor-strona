﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EventPage.Models
{
    public class Rewards
    {
        [Key]
        public int RewardsID { get; set; }
        [Required]
        public string Name { get; set; }

        public string ImagePath { get; set; }

        public Raffle Raffle { get; set; }
    }
}
