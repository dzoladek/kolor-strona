﻿using EventPage.Enums;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static EventPage.Enums.FileTypes;

namespace EventPage.Helpers.ControllersHelpers
{
    public static class ImageUploader
    {

        public static string ImageUpload(IHostingEnvironment hostEnv, IFormFile file, string pagePath, FileType type)
        {
            try
            {
                if (pagePath[pagePath.Length - 1] == '/')
                    pagePath.Remove(pagePath.Length - 1, 1);
                var targetDirectory = Path.Combine(hostEnv.WebRootPath, pagePath);
                var fileName = Guid.NewGuid().ToString() + GetExtension(file.FileName);
                if (CheckExtension(file.FileName,type))
                {
                    var savePath = Path.Combine(targetDirectory, fileName);
                    if (!Directory.Exists(targetDirectory))
                        Directory.CreateDirectory(targetDirectory + "/");

                    using (var fileStream = new FileStream(savePath, FileMode.Create))
                    {
                        file.CopyTo(fileStream);
                    }
                    return "/" + pagePath + "/" + fileName;
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
            return "";
        }
    }
}
