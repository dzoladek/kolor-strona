﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EventPage.Enums
{
    public static class FileTypes
    {
        public enum FileType
        {
            All, Images
        }

        public static string[] Extensions(FileType choice)
        {
            if (choice == FileType.All)
                return new string[] { "*" };
            if (choice == FileType.Images)
                return new string[] { "png", "jpg", "jpeg", "gif" };
            else
                return new string[] { "*" };
        }

        public static bool CheckExtension(string name, FileType choice)
        {
            string patternValid = @"^.+\.[^\\]+$";
            
            var match = Regex.Match(name, patternValid);
            if (match.Success)
            {
                string extension = GetExtension(name).Remove(0,1);

                foreach (var ext in Extensions(choice))
                {
                    if (ext == extension)
                        return true;
                }
            }
            return false;
        }

        public static string GetExtension(string name)
        {
            string patternExtension = @"\.[a-zA-Z0-9]*$";
            var match = Regex.Match(name, patternExtension);
            return match.Value;
        }
    }
}
