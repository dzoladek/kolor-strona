﻿// Write your Javascript code.
var ua = window.navigator.userAgent;
    $(function () {
        jQuery.scrollSpeed(80, 800);
    });

(function ($) {

    jQuery.scrollSpeed = function (step, speed, easing) {
        var $document = $(document),
            $window = $(window),
            $body = $('html, body'),
            option = easing || 'default',
            root = $(window).scrollTop(),
            scroll = false,
            scrollY,
            scrollX,
            view;

        if (window.navigator.msPointerEnabled)

            return false;

        $window.on('mousewheel DOMMouseScroll', function (e) {

            var deltaY = e.originalEvent.wheelDeltaY,
                detail = e.originalEvent.detail;
            scrollY = $document.height() > $window.height();
            scrollX = $document.width() > $window.width();
            scroll = true;

            if (scrollY) {

                view = $window.height();

                if (deltaY < 0 || detail > 0)

                    root = (root + view) >= $document.height() ? root : root += step;

                if (deltaY > 0 || detail < 0)

                    root = root <= 0 ? 0 : root -= step;

                $body.stop().animate({

                    scrollTop: root

                }, speed, option, function () {

                    scroll = false;

                });
            }

            if (scrollX) {

                view = $window.width();

                if (deltaY < 0 || detail > 0)

                    root = (root + view) >=  $document.width() ? root : root += step;

                if (deltaY > 0 || detail < 0)

                    root = root <= 0 ? 0 : root -= step;

                $body.stop().animate({

                    scrollLeft: root

                }, speed, option, function () {

                    scroll = false;

                });
            }

            return false;

        }).on('scroll', function () {

            if (scrollY && !scroll) root = $window.scrollTop();
            if (scrollX && !scroll) root = $window.scrollLeft();
            

        }).on('resize', function () {

            if (scrollY && !scroll) view = $window.height();
            if (scrollX && !scroll) view = $window.width();

        });
    };

    jQuery.easing.default = function (x, t, b, c, d) {

        return -c * ((t = t / d - 1) * t * t * t - 1) + b;
    };

})(jQuery);

var previouScroll = -100;
var effectScroll = $(window).scrollTop()-$(window).height();
var hideAnimatedOffset = -$(window).height() / 10;
var animatedItems = $('.animated:not(.animated-child), [child-animation]');
var RestartAnimation = function (item) {
    $(item).addClass("animation-restart");
    setTimeout(function () {
        $(item).removeClass("animation-restart");
    }, 15);
};
var UnHide = function (item) {
    setTimeout(function () {
        $(item).removeClass("hidden");
    }, 15);
};

var getCSS = function (prop, fromClass) {

    var $inspector = $("<div>").css('display', 'none').addClass(fromClass);
    $("body").append($inspector); // add to DOM, in order to read the CSS property
    try {
        return $inspector.css(prop);
    } finally {
        $inspector.remove(); // and remove from DOM
    }
};
var liteScroll = function (e) {
    var currentScroll = $(window).scrollTop();
    var windowHeight = $(window).height();
    let bestChoice = -1;
    let bestVal = 0;
    let margin = 25;
    let navbarHeight = $('.navbar').outerHeight();
    offSetManager(".navbar-header,.navbar-container,.navbar,.navbar-brand");
    offSetManagerMobile(".navbar");
    if (Math.abs(previouScroll - currentScroll) > margin) {
        var x = $("section");
        for (var i = x.length - 1; i >= 0; i--) {
            var rect = x[i].getBoundingClientRect();
            var percentage = x[i].getBoundingClientRect().top
            var val = (rect.top + rect.bottom - 3 * margin);
            if (bestChoice === -1 || (bestVal < -margin) || (val < bestVal && val >= -margin)) {
                bestChoice = i;
                bestVal = val;
            }
            else if (val > bestVal)
                break;
        }
        $(".nav-link").blur();
        $(".nav-link").removeClass("currentSection-font");
        $(".nav-link").removeClass("currentSection-color");
        $(".nav-link[href='#" + $(x[bestChoice]).attr("id") + "']").addClass("currentSection-color");
        $(".currentSection-color").not(".navbar-brand").addClass("currentSection-font");
        previouScroll = currentScroll;
    }
}

var scroll = function (e) {
    var currentScroll = $(window).scrollTop();
    var windowHeight = $(window).height();
    let bestChoice = -1;
    let bestVal = 0;
    let margin = 25;
    let navbarHeight = $('.navbar').outerHeight();
    offSetManager(".navbar-header,.navbar-container,.navbar,.navbar-brand");
    offSetManagerMobile(".navbar");
    if (Math.abs(previouScroll - currentScroll) > margin) {
        var x = $("section");
        for (var i = x.length - 1; i >= 0; i--) {
            var rect = x[i].getBoundingClientRect();
            var percentage = x[i].getBoundingClientRect().top
            var val = (rect.top + rect.bottom - 3 * margin);
            if (bestChoice === -1 || (bestVal < -margin) || (val < bestVal && val >= -margin)) {
                bestChoice = i;
                bestVal = val;
            }
            else if (val > bestVal)
                break;
        }
        $(".nav-link").blur();
        $(".nav-link").removeClass("currentSection-font");
        $(".nav-link").removeClass("currentSection-color");
        $(".nav-link[href='#" + $(x[bestChoice]).attr("id") + "']").addClass("currentSection-color");
        $(".currentSection-color").not(".navbar-brand").addClass("currentSection-font");
        previouScroll = currentScroll;
    }

    if (currentScroll - effectScroll > 50) {

        var activationPoint = windowHeight;
        var previousActivationPoint = windowHeight - (currentScroll - effectScroll);
        for (let item of animatedItems) {
            var pos = $(item)[0].getBoundingClientRect().top;
            var childAnimation = $(item).attr("child-animation");
            if (pos > windowHeight * 2)
                break;
            else if (pos <= windowHeight && $(item).hasClass("hidden")) {
                UnHide(item);

                if ($(item).hasClass("animated"))
                    RestartAnimation(item);

                if (childAnimation !== undefined) {
                    $(item).children().each(function () {
                        $(this).addClass("hidden");
                    });
                    var time = parseFloat(getCSS("animation-duration", "animated"));
                    var animationStyle = $(item).attr("animation-style");
                    var AddAnimationToChild = function (child, childClassAnimation) {
                        UnHide(child);
                        if (!$(child).hasClass("animated")) {
                            $(child).addClass(childClassAnimation);
                            $(child).addClass("animated");
                            $(child).addClass("animated-child");
                        }
                        else {
                            RestartAnimation(child);
                        }
                    };

                    if (animationStyle === undefined || animationStyle === "together") {
                        $(item).children().each(function () {
                            AddAnimationToChild(this, childAnimation);
                        });
                    }
                    else if (animationStyle === "race") {
                        let iteration = 0;
                        $(item).children().each(function () {
                            setTimeout(AddAnimationToChild, iteration++ / 4 * time * 1000, this, childAnimation);
                        });
                    }
                    else if (animationStyle === "fastrace") {
                        let iteration = 0;
                        $(item).children().each(function () {
                            setTimeout(AddAnimationToChild, iteration++ / 8 * time * 1000, this, childAnimation);
                        });
                    }
                    else if (animationStyle === "random") {
                        var array = [];
                        var count = $(item).children().length;
                        for (var c = 0; c < count; c++)
                            array.push(c);

                        $(item).children().each(function () {
                            var choice = Math.floor(Math.random() * array.length);
                            setTimeout(AddAnimationToChild, array.splice(choice, 1) / 4 * time * 1000, this, childAnimation);
                        });
                    }
                }
            }
        }
        effectScroll = currentScroll;
    }
    else if (currentScroll - effectScroll < hideAnimatedOffset) {
        setTimeout(function () {
            animatedItems.each(function () {
                var pos = $(this)[0].getBoundingClientRect().top;
                if (pos > windowHeight * 1.03) {
                    $(this).addClass("hidden");
                    if ($(this).attr("child-animation") !== undefined)
                        $(this).children().addClass("hidden");
                }
            });
        }, 50);
        $(".toggle").each(function () {
            var pos = $(this)[0].getBoundingClientRect().top;
            if (pos > windowHeight * 1.03) {
                $(this).hide();
            }
            effectScroll = currentScroll;
        });
    }
};
function offSetManagerMobile(elements) {
    var yOffset = 0;
    var currYOffSet = window.pageYOffset;

    if ($(window).height() <= 400) {
        $(elements).removeClass("fixed-theme");
        $(elements).addClass("mobile");
    }
    else
        $(elements).removeClass("mobile");
}
function offSetManager(elements) {
    var yOffset = 0;
    var currYOffSet = window.pageYOffset;
    
    if (currYOffSet >= 5)
        $(elements).addClass("fixed-theme");
    else
        $(elements).removeClass("fixed-theme");
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

var initMap = function () {
    if ($("#map").length === 0)
        return;
    var myLatLng = { lat: 54.39570149999999, lng: 18.573394300000018 };

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 14,
        center: myLatLng,
        disableDefaultUI: true,
        styles: [{ "featureType": "all", "elementType": "labels.text.fill", "stylers": [{ "color": "#ffffff" }] }, { "featureType": "all", "elementType": "labels.text.stroke", "stylers": [{ "color": "#000000" }, { "lightness": 13 }] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [{ "color": "#000000" }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#144b53" }, { "lightness": 14 }, { "weight": 1.4 }] }, { "featureType": "landscape", "elementType": "all", "stylers": [{ "color": "#08304b" }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#0c4152" }, { "lightness": 5 }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#000000" }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#0b434f" }, { "lightness": 25 }] }, { "featureType": "road.arterial", "elementType": "geometry.fill", "stylers": [{ "color": "#000000" }] }, { "featureType": "road.arterial", "elementType": "geometry.stroke", "stylers": [{ "color": "#0b3d51" }, { "lightness": 16 }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#000000" }] }, { "featureType": "transit", "elementType": "all", "stylers": [{ "color": "#146474" }] }, { "featureType": "water", "elementType": "all", "stylers": [{ "color": "#021019" }] }]
    });

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'Pokochaj UG!'
    });
};

$(function () {
    $('a[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top - $('.navbar').outerHeight()
                }, 700);
                return false;
            }
        }
    });
});

$.fn.extend({
    animateCss: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.addClass('animated ' + animationName).one(animationEnd, function () {
            $(this).removeClass('animated ' + animationName);
        });
    }
});
(function ($) {
    $.fn.getFormData = function () {
        var data = {};
        var dataArray = serializeArray();
        for (var i = 0; i < dataArray.length; i++) {
            data[dataArray[i].name] = dataArray[i].value;
        }
        return data;
    }
})(jQuery);

if (typeof window.orientation === 'undefined') {
    window.onscroll = function () {
        scroll();
    };
    window.onresize = function () {
        scroll();
    };
}
else {
    window.onscroll = function () {
        liteScroll();
    };
    window.onresize = function () {
        liteScroll();
    };
}
$(document).ready(function () {
    if (typeof window.orientation === 'undefined') {
        animatedItems.each(function () {

            var windowHeight = $(window).height();
            if ($(this).attr("child-animation")) {
                $(this).addClass("hidden");
                $(this).children().addClass("hidden");
            }
            else {
                var pos = ($(this).parent()[0].getBoundingClientRect().top + $(this).parent()[0].getBoundingClientRect().bottom) / 2;
                if (pos > windowHeight)
                    $(this).addClass("hidden");
                else
                    RestartAnimation(this);
            }

        });
        scroll();
    }
    else {
        liteScroll();
    }
    initMap();
    $('.sponsor-hover').mouseenter(function () {
        if (!$(this).hasClass("animated")){
            $(this).addClass("animated");
            $(this).addClass("pulse");
        }
        else{
            RestartAnimation($(this));
        }
    });
    $('.Person').
        mouseenter(function () {
        $(this).find(".front").addClass("splited");
        $(this).find(".back").addClass("splited");
        $(this).addClass("person-outline");
    })
    $('.Person').mouseleave(() => {
        $('.front, .back').removeClass("splited");
        $(".Person").removeClass("person-outline");
    })

    if (1487147400000 > Date.now() && $("section").length > 1) {
        $("#survey-form").removeClass("toggle");
        $("#survey-form").addClass("fullyhidden");
        $("#survey-form").hide();
        $("#survey-filled").addClass("toggle");
        $("#survey-filled").removeClass("fullyhidden");
        $("#survey-filled").text("Ankieta będzie dostępna po rozpoczęciu się wydarzenia");
    }
    else {
        RemoveRegisterIfExisitng();
        RemoveFilledSurveys();
    }

    ToggleIframe($(this), "https://zakochajsiewitzug.evenea.pl?out=1&source=event_iframe");
    if ($("section").length < 2)
        $(".navbar").hide();
});


function PostForm(frm, successAction, errorAction) {
    
    frm.addClass("disabled");
    var animationInterval;
    var startLoading = function () {
        frm.find("[type=submit]").addClass("disabled");
        frm.children().addClass("dimmed");
        frm.append('<div class="overlay"><div class="sk-three-bounce"> <div class="sk-child sk-bounce1"></div> <div class="sk-child sk-bounce2"></div> <div class="sk-child sk-bounce3"></div> </div></div>');
        
    }();
    var endLoading = function () {
        frm.find(".overlay").remove();
        frm.children().removeClass("dimmed");
        $(frm).find("[type=submit]").removeClass("disabled");
    };

    $.ajax({
        type: "POST",
        url: frm.attr('action'),
        data: frm.serialize(),
        success: function (response) {
            try {
                let serialized = frm.serializeArray();
                if (typeof response === 'string' || response instanceof String)
                    response = JSON.parse(response);
                if (response.success) {
                    frm.find("input").val('');
                    successAction(response, serialized);
                }
                else {
                    errorAction(response);
                    if (response.error !== undefined)
                        console.log(response.error);
                }
                endLoading();
            }
            catch (e) {
                console.log(e);
                errorAction(e);
                endLoading();
            }

        }, 
        error: function (data) {
            console.log(data);
            errorAction();
            endLoading();
        }
    });
}
function SendNewEmail() {
    let frm = $('#contact-form');
    let accept = function (data) {
        $('#email-success').modal('show');
    };
    let decline = function () {
        $('#email-error').modal('show');
    };
    PostForm(frm,accept,decline);
    return false;
}

function SendNewSurvey() {
    $('#survey-form').children('[name=cookie]').remove();
    $('#survey-form').append("<input type='hidden' name='cookie' value='"+ getCookie("surveyuser")+"'/>");
    let frm = $('#survey-form');
    let accept = function (data,serialized) {
        setCookie("surveyuser", data.cookie, 90);
        $('#survey-success').modal('show');
        $("#survey-filled").addClass("toggle");
        let selected = serialized.filter(function (obj) {
            if (obj.name === "EventPartID")
                return obj;
        })[0];
        RemoveFilledSurveys(selected.value);
        RemoveRegisterIfExisitng();
    };
    let decline = function (data) {
        $('#survey-error').modal('show');
        if (data !== undefined) {
            if (data.alreadyFilled)
                RemoveFilledSurveys($("#EventPartID option:selected").attr("value"));
            if (data.cookie !== undefined) {
                if (data.cookie === "-1")
                    setCookie("surveyuser", "", 90);
                else
                    setCookie("surveyuser", data.cookie, 90);
                RemoveRegisterIfExisitng();
            }
        }
    };
    PostForm(frm, accept, decline);
    return false;
}

function RemoveFilledSurveys(newValue) {
    var filled = [];
    try {
        filled = JSON.parse(getCookie("surveyfilled535"));   
    }
    catch(e)
    {
        console.log("Error while reading cookie, using empty array as new value");
    }
    if(newValue !== undefined)
        filled.push(newValue);
    setCookie("surveyfilled535", JSON.stringify(filled), 90);
    $("#EventPartID").children().each(function () {
        $(this).removeAttr("selected");
        var value = $(this).attr("value");
        if (filled.indexOf(value) !== -1) {
            $(this).attr("disabled", "");
            $(this).hide();
        }
    });
    $("#EventPartID").val($("#EventPartID option:not([disabled]):first").val());
    if ($("#EventPartID").children(":not([disabled])").length === 0) {
        $("#survey-form").removeClass("toggle");
        $("#survey-form").addClass("fullyhidden");
        $("#survey-form").hide();
        $("#survey-filled").addClass("toggle");
        $("#survey-filled").removeClass("fullyhidden");
    } 
}
function RemoveRegisterIfExisitng() {
    if (getCookie("surveyuser") === "") {
        setCookie("surveyfilled535", "", 90);
        RemoveFilledSurveys();
        $('#survey-form').children(".userdata").removeClass("fullyhidden").attr("required");
    }
    else
        $('#survey-form').children(".userdata").addClass("fullyhidden").find("input").removeAttr("required");
}

function ToggleIframe(thiselem,url) {
    if(!(thiselem instanceof jQuery))
        thiselem = $(thiselem);
    var toToggle = thiselem.parents("section").find(".toggle");
    
    if (toToggle.find("iframe").length === 0) {
        $('<iframe>', {
            src: url,
            id: 'evenea',
            frameborder: 0,
            onload: "$(this).height($(this.contentWindow.document.body).find(\'div\').first().height());",
            scrolling: 'no'
        }).prependTo(toToggle).addClass("pt-5").attr("width","100%"); 
        $("iframe").iFrameResize({ widthCalculationMethod: "max" });
    }
    toToggle.toggle("slow");
}
