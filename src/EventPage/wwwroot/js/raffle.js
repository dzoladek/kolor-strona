﻿var showedParticipants = 9;
var STOP_SPEED = 2000; 
var blocked_speed = GetBlockedSpeed();


function GetBlockedSpeed() {
    let stop = 1 / (STOP_SPEED / 20);

    for (var i = 1 ; i <= STOP_SPEED; i++) {

        var d = 1 / (i / 20);

        for (var y = 0; y <= (showedParticipants >> 1) ; y++) {

            d -= Math.pow(Math.pow(d * 0.25, 0.07) / 400, 0.8);

            if (d <= stop)
                return i-1;
        }
    }

    return 0;
}

$(".reward").click(function () {
    if (!$(this).hasClass("used")) {
        clearTimeout(changeTimeout);
        $(".raffle-app").children().remove();
        if ($(this).hasClass("selected")) {
            $(".reward").removeClass("dimmed selected");
            HideRaffle();
        }
        else {
            $(".reward").removeClass("selected");
            $(".reward").addClass("dimmed");

            $(this).removeClass("dimmed");
            $(this).addClass("selected");
            LoadRaffle();
        }
    }
});

$.fn.extend({
    style: function (styleObj) {
        for (let prop in styleObj)
            $(this).css(prop, styleObj[prop]);
    }
});

if (showedParticipants <= 1)
    showedParticipants = 3;
if (showedParticipants % 2 === 0)
    showedParticipants++;

var animations = [];
for (var b = 0; b < Math.ceil(showedParticipants / 2); b++) {
    let start = b / Math.ceil(showedParticipants / 2);
    let end = (b + 1) / Math.ceil(showedParticipants / 2);
    let scaleStart = Math.pow(b / Math.ceil(showedParticipants / 2), 0.25);
    let scaleEnd = Math.pow((b + 1) / Math.ceil(showedParticipants / 2), 0.25);

    let endIterator = (showedParticipants - b);
    animations[b] = {
        from: {
            "opacity": start,
            "scale": scaleStart
        },
        to: {
            //"scale" : end,
            "opacity": end,
            ease: Bounce,
            scale:scaleEnd
        }
    };
    animations[endIterator] = {
        from: {
            //"scale" : end,
            "opacity": end,
            scale: scaleEnd
        },
        to: {
            "opacity": start,
            "scale": scaleStart,
            ease: Bounce
        }
    };
}
animations[showedParticipants].to["onComplete"] = function () {
    $(this["_targets"]).detach().appendTo("#hidden-container").removeAttr("style");
};
animations[showedParticipants].to["height"] = '0px';
animations[showedParticipants].to["scaleY"] = '0';

var coun;
var z = function (x, s) {
    let r = $(".selected").attr("data-id");
    let c = { rn: "none", fn: "none" };
    let f = x.attr("data-cid");
    let p = x.attr("participant-id");
    console.log("speed " + s)
    if (c.rn === r) {
        if (coun === undefined) {
            if (s <= blocked_speed) {
                let selP = $("#hidden-container > .participant[data-cid=" + c.fn + "]");
                coun = selP.eq(Math.floor(Math.random() * selP.length)).attr("participant-id");
            }
        }
        else {
            if (f === c.fn && p === coun)
                return null;

            if (s > blocked_speed) {
                if (localStorage.selected === undefined) {
                    x = $("#hidden-container > .participant[participant-id=" + coun + "]");
                    coun = undefined;
                    console.log("zamiana");
                    localStorage.selected = true;
                }
            }
        }
    }
    return x.detach();
};

var participantsSelector, minSpeed, changeTimeout, changeTimeoutTime, inRaffleCount, selected;
var acceleration = 0;
function LoadRaffle() {
    acceleration = 0;
    selected = false;
    clicked = false;
    inRaffleCount = 0;
    minSpeed = false;
    let mainApp = $(".raffle-app").empty().addClass("hidden");
    let participants = $('.participant');
    let repeat = ((showedParticipants * 20) / participants.length > 1) ? Math.ceil((showedParticipants * 20) / participants.length) : 1;
    let hidden; $("<div id='hidden-container'></div>").appendTo(".raffle-app");
    let shown = $("<div id='shown-container'></div>").appendTo(".raffle-app");
    participants.each(function () {
        let tickets = parseInt($(this).attr("data-tickets"));
        if ($('.reward[data-winner=' + $(this).attr("data-id") + ']').length === 0) {
            for (let i = 0; i < repeat * tickets; i++) {
                let b = $(this).clone();
                let text = b.text();
                b.text("");
                let name = $("<div></div>").addClass("participant-text").text(text);
                var x = b.appendTo("#hidden-container");
                x.attr("participant-id", inRaffleCount++);
                x.attr("data-id", $(this).attr("data-id"));
                x.append($("<div></div>").addClass("participant-text").text(text));
            }
        }
    });
    if ($("#hidden-container").children().length === 0)
        selected = true;
    else {
        mainApp.removeClass("hidden");
        setTimeout(change, 100);
    }
}

function change() {

    if (!selected && $("#shown-container, #hidden-container").children().length > 0) {
        let speed = (acceleration === 0) ? 40000 : Math.ceil(Math.abs(1 / (acceleration)) * 20);
        let changeAccl = Math.sign(acceleration) * Math.pow(Math.pow(Math.abs(acceleration) * 0.25, 0.07) / 400, 0.8);
        let b = Math.pow(Math.pow(changeAccl,12)* 400,1.93)*4;
        acceleration = (Math.sign(acceleration - changeAccl) !== Math.sign(acceleration) && !clicked) ? 0 : acceleration - changeAccl;
        if (speed > STOP_SPEED && minSpeed && !clicked) {
            selected = true;
            let winner = $("#shown-container").find(".participant:not(.hidden)").eq(Math.floor(showedParticipants / 2)).addClass("winner");
            let reward = $(".selected").removeClass("selected").addClass("used").attr("data-winner",winner.attr("data-id"));
            $(".reward").removeClass("dimmed");
            $.ajax({
                type: "POST",
                url: "/RaffleAPI/Save",
                data: {
                    WinnerID:winner.attr("data-id"),
                    RewardID:reward.attr("data-id")
                },
                success: function (response) {
                    try {
                        if (typeof response === 'string' || response instanceof String)
                            response = JSON.parse(response);
                        if (response.success) {
                            console.log("success");
                        }
                        else {
                           if (response.error !== undefined)
                                console.log(response.error);
                        }
                    }
                    catch (e) {
                        console.log(e);
                    }

                },
                error: function (data) {
                    console.log(data);
                }
            });
        }
        else {

            if (clicked)
                minSpeed = false;
            else if (speed < 100)
                minSpeed = true;
            $("#hidden-container").children().removeAttr("style");
            let end = false;
            do {
                var random = Math.floor(Math.random() * ($("#hidden-container").children().length + 1));
                var elem  = $("#hidden-container > .participant").eq(random);
                var x = z(elem, speed);
                if (x !== null) {
                    let shown = $("#shown-container").find(".participant").length;
                    if (shown <= showedParticipants) {
                        if (acceleration < 0)
                            $("#shown-container").prepend(x);
                        else
                            $("#shown-container").append(x);
                    }
                    else
                        end = true;
                }
            }
            while (!end);

            let height = parseFloat($("#shown-container").children(".participant")[0].getBoundingClientRect().height) * ((Math.sign(acceleration) < 0)? -1 : 0);
  
            TweenMax.fromTo("#shown-container .participant", speed / 1000, { y: height}, { y: 0, ease: Sine });

            for (let i = 0; i <= showedParticipants; i++) {
                if (acceleration < 0)
                    TweenMax.fromTo("#shown-container .participant:eq(" + i + ")", speed / 1000, animations[i].from, animations[i].to);
                else
                    TweenMax.fromTo("#shown-container .participant:eq(" + i + ")", speed / 1000, animations[showedParticipants - i].from, animations[showedParticipants - i].to);
            }

            changeTimeout = setTimeout(change, speed+4);
            changeTimeoutTime = Date.now() + speed+4;
        }

    }
}

var previousPos;
var clicked = false;
function ChangeSpeed(currentMouse) {
    
    let height = $(".raffle-app .participant")[0].getBoundingClientRect().height;
    let AcclChange = (previousPos.pageY - currentMouse.pageY) / (Date.now() - previousPos.timestamp) / 20;
    let speed = (acceleration === 0) ? 40000 : Math.ceil(Math.abs(1 / (acceleration)) * 40);
    if (AcclChange !== 0 && (speed < blocked_speed || speed >= blocked_speed && !minSpeed)) {
        let previousAccel = acceleration;
        acceleration += AcclChange;
        if (acceleration > 1) acceleration = 1;
        if (acceleration < -1) acceleration = -1;
        speed = (acceleration === 0) ? 40000 : Math.ceil(Math.abs(1 / (acceleration)) * 40);
        if (Date.now() + speed !== changeTimeoutTime) {
            clearTimeout(changeTimeout);
            changeTimeout = setTimeout(function () { change(); }, speed);
            changeTimeoutTime = Date.now() + speed;
        }
    }
}
$(".raffle-app").mousedown(function (e) {
    e.preventDefault();
    clicked = true;
    console.log("clicked")
});
$(".raffle-app").mousemove(function (e) {
    if (clicked === true && (previousPos === undefined || previousPos.timestamp < Date.now())) {
        
        if (previousPos !== undefined && previousPos.pageY !== e.pageY && !selected) {
            ChangeSpeed(e);
        }
        if (previousPos === undefined || (previousPos.pageX !== e.pageX || previousPos.pageY !== e.pageY))
            previousPos = { pageX: e.pageX, pageY: e.pageY, timestamp: Date.now() };

       // var elem = $(".raffle-app")[0].getBoundingClientRect();
       // if (e.pageY < elem.top || e.pageY > elem.bottom || e.pageX < elem.left || e.pageX > elem.right) {
        //    clicked = false;
        //    console.log("zmiana");
       // }
    }
});

$(document).mouseup(function (e) {
    clicked = false;
    previousPos = undefined;
    console.log("false");
});


function NewResult() {

}
function HideRaffle() {

}